.PHONY: install uninstall set-default

uninstall:
	rm -rv /usr/share/plymouth/themes/frontline || true

install: uninstall
	mkdir /usr/share/plymouth/themes/frontline
	cp -v *.script *.plymouth *.png /usr/share/plymouth/themes/frontline/

set-default:
	update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/frontline/frontline.plymouth 100
	update-alternatives --config default.plymouth
	update-initramfs -u
