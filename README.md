Frontline Plymouth Theme
=====================

Ubuntu Server 22.04 à la Windows 10 boot splash theme for Plymouth

![Frontline Plymouth Theme preview](frontline-preview.gif)

Creative Commons CCBY (2022), Junior A. S. de Arruda <junior (a) datapro net br>  
https://datapro.net.br/

Based on

- Pure CSS Windows10 Loader by Fernando de Almeida Faria  
  https://codepen.io/feebaa/pen/PPrLQP
and
- Plymouth theming guide (part 4)  
  http://brej.org/blog/?p=238

Documentation about scripting available at
- https://www.freedesktop.org/wiki/Software/Plymouth/Scripts/
and
- https://sources.debian.org/src/plymouth/0.9.2-4/src/plugins/splash/script/

### Installation

    wget https://gitlab.com/jayarruda/frontline-plymouth/-/archive/master/frontline-plymouth-master.tar.gz
    tar xvaf frontline-plymouth-master.tar.gz
    cd frontline-plymouth-master
    make install   # as root or with sudo
    make set-default   # as root or with sudo
    # reboot to see the new theme

### License

This theme is licensed under a Creative Commons CCBY license.
